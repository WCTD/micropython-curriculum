from microbit import *
from microbit import sleep as slp
import radio

name = "James"
names = ["John","Jenny","Jake"]

radio.config(power=2)

while True:
    radio.on()
    while accelerometer.is_gesture("face up"):
        # Recieve Names
        incoming = radio.receive()
        if incoming != None and incoming not in names:
            display.scroll(incoming)
            names = names + [incoming]
        else:
            display.show("X")

    while accelerometer.is_gesture("face down"):
        # Send Names
        radio.send(name)
        display.show("S")

    
    radio.off()
    index = len(names)
    while accelerometer.is_gesture("up"):
        display.scroll((names + [name])[index])
        if index > 0 and button_a.is_pressed():
            index = index - 1
        elif index < len(names) and button_b.is_pressed():
            index = index + 1