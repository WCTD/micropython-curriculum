ount from microbit import *

while True:
    if accelerometer.is_gesture("face up"):
        display.show("f")
    elif accelerometer.is_gesture("face down"):
        display.show("F")
    elif accelerometer.is_gesture("up"):
        display.show("u")
    elif accelerometer.is_gesture("down"):
        display.show("d")
    elif accelerometer.is_gesture("left"):
        display.show("l")
    elif accelerometer.is_gesture("right"):
        display.show("r")